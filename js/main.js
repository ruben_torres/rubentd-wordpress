var counter = 0;
var adj = ["sexy", "responsive", "trendy", "intuitive", "HTML5", "CSS3", "semantic", "fast"];
var interval;
var fastInterval;
var msec = 800;
var msec2 = 100;

$(document).ready( function(){

	setTimeout(function(){ initialSwap(); }, 1000);

	$("#adjectives").mouseover( function(){
		window.clearInterval(fastInterval);
		swapAdj();
	});

	$("#adjectives").click( function(){
		window.clearInterval(fastInterval);
		window.clearInterval(interval);
		rotateAdjectives();
	});

	$("#adjectives").mouseout( function(){
		window.clearInterval(interval);
	});

	$("#note-top").click( function(){
		$(this).toggleClass("down");
	});

	setTimeout( function(){ $("#note-top").css('display','inline-block'); }, 5000);

	/* 
	 *Hey, that's cheating! you are looking at the source code ;) 
	 */
	var egg = new Konami();
	egg.code = function() { 
		ShowToasty();
	}
	egg.load();
});

function ShowToasty(){
	var toasty = document.getElementById('toasty');
	toasty.play();
	$("#dan").addClass("show-dan");
	setTimeout( function(){ $("#dan").removeClass("show-dan"); }, 1000);
}

function initialSwap(){
	rotateAdjectivesAndCheck();
	fastInterval = setInterval(function(){ rotateAdjectivesAndCheck(); } , msec2);
}

function swapAdj(){
	rotateAdjectives();
	interval = setInterval(function(){ rotateAdjectives(); } , msec);
}

function rotateAdjectives(){
	increaseCounter();
	var nextAdj = adj[counter];
	$("#adjectives").html(nextAdj);
}

function rotateAdjectivesAndCheck(){
	rotateAdjectives();
	if(counter == 0){
		window.clearInterval(fastInterval);
	}
}

function increaseCounter(){
	if(counter < adj.length - 1){
		counter++;
	}else{
		counter = 0;
	}
}